use chrono::{DateTime, Utc};
use chrono_tz::UTC;
use rcore::{
    IntermediateLocation, Journey, JourneysResponse, Leg, Line, Location, Mode, Operator, Place,
    Product, Station, Stop,
};
use serde::{Deserialize, Serialize};

use std::borrow::Cow;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisLocationsResponse {
    content: MotisLocationsResponseContent,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisLocationsResponseContent {
    guesses: Vec<MotisStation>,
}

impl From<MotisLocationsResponse> for Vec<Place> {
    fn from(locations: MotisLocationsResponse) -> Self {
        locations
            .content
            .guesses
            .into_iter()
            .map(Place::from)
            .collect()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisStation {
    id: String,
    name: String,
    pos: MotisPosition,
}

impl From<MotisStation> for Place {
    fn from(station: MotisStation) -> Self {
        Place::Station(Station {
            id: station.id.clone(),
            name: Some(station.name.clone()),
            location: Some(Location::Point {
                id: Some(station.id),
                name: Some(station.name),
                poi: None,
                latitude: station.pos.lat as f32,
                longitude: station.pos.lng as f32,
            }),
            products: vec![],
        })
    }
}
#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisPosition {
    lat: f64,
    lng: f64,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisJourneysResponse {
    content: MotisJourneysResponseContent,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisJourneysResponseContent {
    connections: Vec<MotisConnection>, // TODO: direct_connection?
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisConnection {
    stops: Vec<MotisStop>,
    transports: Vec<MotisMove>,
    trips: Vec<MotisTrip>,
    attributes: Vec<MotisAttribute>,
    free_texts: Vec<MotisFreeText>,
    // problems: Vec<MotisProblem>,
    // TODO: status?
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisStop {
    station: MotisStation,
    arrival: MotisEventInfo,
    departure: MotisEventInfo,
    enter: bool,
    exit: bool,
}

impl From<MotisStop> for IntermediateLocation {
    fn from(stop: MotisStop) -> IntermediateLocation {
        IntermediateLocation::Stop(Stop {
            place: stop.station.into(),
            departure: DateTime::<Utc>::from_timestamp(stop.departure.time.try_into().unwrap(), 0)
                .as_ref()
                .map(|t| t.with_timezone(&UTC)),
            planned_departure: DateTime::<Utc>::from_timestamp(
                stop.departure.schedule_time.try_into().unwrap(),
                0,
            )
            .as_ref()
            .map(|t| t.with_timezone(&UTC)),
            arrival: DateTime::<Utc>::from_timestamp(stop.arrival.time.try_into().unwrap(), 0)
                .as_ref()
                .map(|t| t.with_timezone(&UTC)),
            planned_arrival: DateTime::<Utc>::from_timestamp(
                stop.arrival.schedule_time.try_into().unwrap(),
                0,
            )
            .as_ref()
            .map(|t| t.with_timezone(&UTC)),
            arrival_platform: Some(stop.arrival.track),
            planned_arrival_platform: Some(stop.arrival.schedule_track),
            departure_platform: Some(stop.departure.track),
            planned_departure_platform: Some(stop.departure.schedule_track),
            cancelled: false,
            remarks: vec![],
        })
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisEventInfo {
    time: u64,
    schedule_time: u64,
    track: String,
    schedule_track: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(tag = "move_type", content = "move")]
pub enum MotisMove {
    Transport(MotisTransport),
    Walk(MotisWalk),
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisTransport {
    range: MotisRange,
    clasz: u8,
    line_id: String,
    name: String,
    provider: String,
    direction: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisWalk {
    range: MotisRange,
}

#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
pub struct MotisRange {
    from: u64,
    to: u64,
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash)]
pub struct MotisTrip {
    range: MotisRange,
    id: MotisTripId,
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash)]
pub struct MotisTripId {
    id: String,
    station_id: String,
    train_nr: u64,
    time: i64,
    target_station_id: String,
    target_time: i64,
    line_id: String,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct MotisAttribute {
    range: MotisRange,
    code: String,
    text: String,
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash)]
pub struct MotisFreeText {
    range: MotisRange,
    code: i32,
    text: String,
    r#type: String,
}

impl From<MotisJourneysResponse> for JourneysResponse {
    fn from(motis: MotisJourneysResponse) -> Self {
        JourneysResponse {
            earlier_ref: motis
                .content
                .connections
                .first()
                .and_then(|c| c.stops.first())
                .map(|e| e.departure.time.to_string()),
            later_ref: motis
                .content
                .connections
                .last()
                .and_then(|c| c.stops.first())
                .map(|e| e.departure.time.to_string()),
            journeys: motis
                .content
                .connections
                .into_iter()
                .map(Into::into)
                .collect(),
        }
    }
}

impl From<MotisConnection> for Journey {
    fn from(motis: MotisConnection) -> Self {
        let num_legs = motis.transports.len();

        let mut legs = Vec::with_capacity(num_legs);

        for transport in motis.transports {
            let range = match &transport {
                MotisMove::Transport(m) => &m.range,
                MotisMove::Walk(m) => &m.range,
            };

            let stops = &motis.stops[range.from as usize..=range.to as usize];
            let origin_stop = &stops[0];
            let destination_stop = &stops[stops.len() - 1];
            let leg = Leg {
                origin: origin_stop.station.clone().into(),
                destination: destination_stop.station.clone().into(),
                departure: DateTime::<Utc>::from_timestamp(
                    origin_stop.departure.time.try_into().unwrap(),
                    0,
                )
                .map(|t| t.with_timezone(&UTC)),
                planned_departure: DateTime::<Utc>::from_timestamp(
                    origin_stop.departure.schedule_time.try_into().unwrap(),
                    0,
                )
                .map(|t| t.with_timezone(&UTC)),
                arrival: DateTime::<Utc>::from_timestamp(
                    destination_stop.arrival.time.try_into().unwrap(),
                    0,
                )
                .map(|t| t.with_timezone(&UTC)),
                planned_arrival: DateTime::<Utc>::from_timestamp(
                    destination_stop.arrival.schedule_time.try_into().unwrap(),
                    0,
                )
                .map(|t| t.with_timezone(&UTC)),
                reachable: true, // TODO
                trip_id: None,   // trip.id.to_string(), // TODO
                line: match &transport {
                    MotisMove::Transport(transport) => Some(Line {
                        name: Some(transport.name.clone()),
                        fahrt_nr: transport.name.split_once(' ').map(|(_, s)| s.to_owned()),
                        mode: MotisClasz::from_integer(transport.clasz)
                            .map(|c| c.to_mode())
                            .unwrap_or(Mode::Unknown),
                        operator: Some(Operator {
                            id: transport.provider.clone(),
                            name: transport.provider.clone(),
                        }),
                        product_name: Some(
                            transport
                                .name
                                .split_once(' ')
                                .map(|(s, _)| s.to_owned())
                                .unwrap_or_else(|| transport.line_id.clone()),
                        ),
                        product: Product {
                            mode: Mode::Unknown, // TODO
                            name: Cow::from(transport.name.clone()),
                            short: Cow::from(
                                transport
                                    .name
                                    .split_once(' ')
                                    .map(|(s, _)| s.to_owned())
                                    .unwrap_or_else(|| transport.line_id.clone()),
                            ),
                        },
                    }),
                    MotisMove::Walk(_) => None,
                },
                direction: match &transport {
                    MotisMove::Transport(transport) => Some(transport.direction.clone()),
                    MotisMove::Walk(_) => None,
                },
                arrival_platform: Some(destination_stop.arrival.track.clone()),
                planned_arrival_platform: Some(destination_stop.arrival.schedule_track.clone()),
                departure_platform: Some(origin_stop.departure.track.clone()),
                planned_departure_platform: Some(origin_stop.departure.schedule_track.clone()),
                frequency: None,
                cancelled: false, // TODO,
                intermediate_locations: stops.iter().cloned().map(Into::into).collect(),
                load_factor: None, // TODO
                remarks: vec![],   // TODO
                walking: match transport {
                    MotisMove::Transport(_) => false,
                    MotisMove::Walk(_) => true,
                },
                transfer: false, // TODO
                distance: None,  // TODO
                #[cfg(feature = "poylines")]
                polyline: None,
            };
            legs.push(leg);
        }

        Journey {
            id: {
                let mut hasher = DefaultHasher::new();
                motis.trips.hash(&mut hasher);
                hasher.finish().to_string()
            },
            legs,
            price: None,
        }
    }
}

/// https://motis-project.de/docs/api/connection.html#clasz-type-integer
#[derive(Debug, Clone, Copy)]
pub enum MotisClasz {
    Flight = 0,
    LongDistanceHighSpeedTrain = 1,
    LongDistanceInterCityTrain = 2,
    LongDistanceBus = 3,
    LongDistanceNightTrain = 4,
    RegionalExpressTrain = 5,
    RegionalTrain = 6,
    MetroTrain = 7,
    SubwayTrain = 8,
    Tram = 9,
    Bus = 10,
    ShipFerry = 11,
    Other = 12,
}

impl MotisClasz {
    fn from_integer(i: u8) -> Option<MotisClasz> {
        match i {
            0 => Some(Self::Flight),
            1 => Some(Self::LongDistanceHighSpeedTrain),
            2 => Some(Self::LongDistanceInterCityTrain),
            3 => Some(Self::LongDistanceBus),
            4 => Some(Self::LongDistanceNightTrain),
            5 => Some(Self::RegionalExpressTrain),
            6 => Some(Self::RegionalTrain),
            7 => Some(Self::MetroTrain),
            8 => Some(Self::SubwayTrain),
            9 => Some(Self::Tram),
            10 => Some(Self::Bus),
            11 => Some(Self::ShipFerry),
            12 => Some(Self::Other),
            _ => None,
        }
    }

    pub(crate) fn from_mode(mode: Mode) -> &'static [MotisClasz] {
        // TODO: Flight?
        match mode {
            Mode::HighSpeedTrain => &[
                MotisClasz::LongDistanceHighSpeedTrain,
                MotisClasz::LongDistanceInterCityTrain,
                MotisClasz::LongDistanceNightTrain,
            ],
            Mode::RegionalTrain => &[MotisClasz::RegionalExpressTrain, MotisClasz::RegionalTrain],
            Mode::SuburbanTrain => &[MotisClasz::MetroTrain],
            Mode::Subway => &[MotisClasz::SubwayTrain],
            Mode::Tram => &[MotisClasz::Tram],
            Mode::Bus => &[MotisClasz::LongDistanceBus, MotisClasz::Bus],
            Mode::Ferry => &[MotisClasz::ShipFerry],
            Mode::Cablecar => &[MotisClasz::Other],
            Mode::OnDemand => &[MotisClasz::Other],
            Mode::Unknown => &[MotisClasz::Other],
        }
    }

    fn to_mode(&self) -> Mode {
        match self {
            MotisClasz::Flight => Mode::Unknown,
            MotisClasz::LongDistanceHighSpeedTrain => Mode::HighSpeedTrain,
            MotisClasz::LongDistanceInterCityTrain => Mode::HighSpeedTrain,
            MotisClasz::LongDistanceBus => Mode::Bus,
            MotisClasz::LongDistanceNightTrain => Mode::HighSpeedTrain,
            MotisClasz::RegionalExpressTrain => Mode::RegionalTrain,
            MotisClasz::RegionalTrain => Mode::RegionalTrain,
            MotisClasz::MetroTrain => Mode::SuburbanTrain,
            MotisClasz::SubwayTrain => Mode::Subway,
            MotisClasz::Tram => Mode::Tram,
            MotisClasz::Bus => Mode::Bus,
            MotisClasz::ShipFerry => Mode::Ferry,
            MotisClasz::Other => Mode::Unknown,
        }
    }
}
