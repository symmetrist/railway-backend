#![doc = include_str!("../README.md")]

mod error;
mod types;
use chrono::Utc;
use error::*;
use types::*;

use async_trait::async_trait;
use rcore::{
    Journey, JourneysOptions, Location, Mode, Place, Provider, Requester, RequesterBuilder,
};
use serde_json::json;
use url::Url;

use std::collections::HashMap;

pub const SPLINE_URL: &str = "https://routing.spline.de/api/";

#[derive(Clone)]
pub struct MotisClient<R: Requester> {
    requester: R,
    base_url: url::Url,
}

impl<R: Requester> MotisClient<R> {
    pub fn new<RB: RequesterBuilder<Requester = R>>(url: Url, requester: RB) -> Self {
        Self {
            requester: requester.build(),
            base_url: url,
        }
    }
}

#[cfg_attr(feature = "rt-multi-thread", async_trait)]
#[cfg_attr(not(feature = "rt-multi-thread"), async_trait(?Send))]
impl<R: Requester> Provider<R> for MotisClient<R> {
    type Error = Error;

    async fn journeys(
        &self,
        from: rcore::Place,
        to: rcore::Place,
        opts: rcore::JourneysOptions,
    ) -> Result<rcore::JourneysResponse, rcore::Error<<R as Requester>::Error, Self::Error>> {
        let interval = if let Some(departure) = opts.departure {
            json!({
                "begin": departure.timestamp(),
                "end": departure.timestamp() + 1,
            })
        } else if let Some(arrival) = opts.arrival {
            json!({
                "begin": arrival.timestamp() - 1,
                "end": arrival.timestamp(),
            })
        } else if let Some(later) = opts.later_than.as_ref().and_then(|s| s.parse::<u64>().ok()) {
            json!({
                "begin": later,
                "end": later + 1,
            })
        } else if let Some(earlier) = opts
            .earlier_than
            .as_ref()
            .and_then(|s| s.parse::<u64>().ok())
        {
            json!({
                "begin": earlier - 1,
                "end": earlier,
            })
        } else {
            let now = Utc::now();
            json!({
                "begin": now.timestamp(),
                "end": now.timestamp() + 1,
            })
        };

        let is_earlier_or_later = opts.earlier_than.is_some() || opts.later_than.is_some();
        let is_now = opts.departure.is_none()
            && opts.arrival.is_none()
            && opts.earlier_than.is_none()
            && opts.later_than.is_none();

        // TODO: How to search by arrival time?
        // TODO: Destination type by station or lat/lon depending on availability.
        // Serach by "arrive by": Set interval on destination.
        let request = json!({
            "content": {
                "destination":{
                    "id": match &to {
                            Place::Station(s) => s.id.to_owned(),
                            Place::Location(l) => match l {
                                Location::Point { id, .. } => id.clone().unwrap_or_default(),
                                Location::Address { .. } => "".to_owned()
                            }
                        },
                    "name": match &to {
                            Place::Station(s) => s.name.clone().unwrap_or_default(),
                            Place::Location(l) => match l {
                                Location::Point { name, .. } => name.clone().unwrap_or_default(),
                                Location::Address { address, .. } => address.to_owned(),
                            }
                        },
                },
                "destination_modes": [
                    {
                        "mode_type": "FootPPR",
                        "mode": {
                            "search_options": {
                                "profile": "default",
                                "duration_limit": 900
                            }
                        }
                    }
                ],
                "destination_type": "InputStation",
                "router": "",
                "search_dir": if opts.arrival.is_some() { "Backward" } else { "Forward" },
                "search_type": "Default",
                "start": {
                    "extend_interval_earlier": (opts.arrival.is_some() && !is_earlier_or_later) || opts.earlier_than.is_some(),
                    "extend_interval_later": (opts.departure.is_some() && !is_earlier_or_later) || opts.later_than.is_some() || is_now,
                    "interval": interval,
                    "min_connection_count": opts.results,
                    "station": {
                        "id": match &from {
                                Place::Station(s) => s.id.to_owned(),
                                Place::Location(l) => match l {
                                    Location::Point { id, .. } => id.clone().unwrap_or_default(),
                                    Location::Address { .. } => "".to_owned()
                                }
                            },
                        "name": match &from {
                                Place::Station(ref s) => s.name.clone().unwrap_or_default(),
                                Place::Location(l) => match l {
                                    Location::Point { name, .. } => name.clone().unwrap_or_default(),
                                    Location::Address { address, .. } => address.to_owned(),
                                }
                            },
                    },
                },
                "start_modes": [
                    {
                        "mode_type": "FootPPR",
                        "mode": {
                            "search_options": {
                                "profile": "default",
                                "duration_limit": 900
                            }
                        }
                    }
                ],
                "start_type": "PretripStart",
                "allowed_claszes": std::collections::HashSet::<Mode>::from(opts.products).into_iter().flat_map(MotisClasz::from_mode).map(|m| *m as u8).collect::<std::collections::HashSet<_>>(),
            },
            "content_type": "IntermodalRoutingRequest",
            "destination": {
                "target": "/intermodal",
                "type": "Module",
            }
        })
        .to_string();

        let response = self
            .requester
            .post(
                &self.base_url,
                request.as_bytes(),
                HashMap::from([
                    ("Accept", "application/json"),
                    ("Content-Type", "application/json"),
                ]),
            )
            .await
            .map_err(rcore::Error::Request)?;

        let response: MotisJourneysResponse = serde_json::from_slice(&response)
            .map_err(|e| rcore::Error::Provider(Error::Json(e)))?;

        Ok(response.into())
    }

    async fn locations(
        &self,
        opts: rcore::LocationsOptions,
    ) -> Result<rcore::LocationsResponse, rcore::Error<<R as Requester>::Error, Self::Error>> {
        // TODO: Also do address search.
        let request = json!({
            "content_type": "StationGuesserRequest",
            "content": {
                "guess_count": opts.results,
                "input": opts.query
            },
            "destination": {
                "target": "/guesser",
                "type": "Module"
            }
        })
        .to_string();

        let response = self
            .requester
            .post(
                &self.base_url,
                request.as_bytes(),
                HashMap::from([
                    ("Accept", "application/json"),
                    ("Content-Type", "application/json"),
                ]),
            )
            .await
            .map_err(rcore::Error::Request)?;

        let response: MotisLocationsResponse = serde_json::from_slice(&response)
            .map_err(|e| rcore::Error::Provider(Error::Json(e)))?;

        Ok(response.into())
    }

    // Note: This method is not guaranteed to find the same journey. But I think this is the best we can do as search.ch does not provide a refresh-API.
    // TODO: Use `trip_to_connection` module instead.
    async fn refresh_journey(
        &self,
        journey: &Journey,
        opts: rcore::RefreshJourneyOptions,
    ) -> Result<rcore::RefreshJourneyResponse, rcore::Error<<R as Requester>::Error, Self::Error>>
    {
        // Note: Each journey should have at least one leg.
        let from = &journey.legs[0].origin;
        let to = &journey.legs[journey.legs.len() - 1].destination;
        let jopts = JourneysOptions {
            // In most cases, the journey to refresh is likely the first one. Relax this requirement a bit.
            results: 3,
            departure: journey.legs[0].planned_departure,
            stopovers: opts.stopovers,
            tickets: opts.tickets,
            tariff_class: opts.tariff_class,
            language: opts.language,
            #[cfg(feature = "polylines")]
            polylines: opts.polylines,
            ..Default::default()
        };
        self.journeys(from.clone(), to.clone(), jopts)
            .await?
            .journeys
            .into_iter()
            .find(|j| j.id == journey.id)
            .clone()
            .ok_or(rcore::Error::Provider(Error::RefreshJourneyNotFound))
    }
}

#[cfg(test)]
mod test {
    use rcore::{HyperRustlsRequesterBuilder, JourneysOptions, LocationsOptions, Station};

    use super::*;

    pub async fn check_search<S: AsRef<str>>(
        search: S,
        expected: S,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let client = MotisClient::new(
            Url::parse(SPLINE_URL).expect("Failed to parse SPLINE_URL"),
            HyperRustlsRequesterBuilder::default(),
        );
        let locations = client
            .locations(LocationsOptions {
                query: search.as_ref().to_string(),
                ..Default::default()
            })
            .await?;
        let results = locations
            .into_iter()
            .flat_map(|p| match p {
                Place::Station(s) => s.name,
                Place::Location(Location::Address { address, .. }) => Some(address),
                Place::Location(Location::Point { name, .. }) => name,
            })
            .collect::<Vec<_>>();
        assert!(
            results.iter().find(|s| s == &expected.as_ref()).is_some(),
            "expected {} to be contained in {:#?}",
            expected.as_ref(),
            results
        );
        Ok(())
    }

    pub async fn check_journey<S: AsRef<str>>(
        from: S,
        to: S,
    ) -> Result<(), Box<dyn std::error::Error>> {
        let client = MotisClient::new(
            Url::parse(SPLINE_URL).expect("Failed to parse SPLINE_URL"),
            HyperRustlsRequesterBuilder::default(),
        );
        let journeys = client
            .journeys(
                Place::Station(Station {
                    id: from.as_ref().to_string(),
                    ..Default::default()
                }),
                Place::Station(Station {
                    id: to.as_ref().to_string(),
                    ..Default::default()
                }),
                JourneysOptions::default(),
            )
            .await?;
        assert!(
            !journeys.journeys.is_empty(),
            "expected journey from {} to {} to exist",
            from.as_ref(),
            to.as_ref()
        );
        Ok(())
    }
    #[tokio::test]
    async fn search_munich() -> Result<(), Box<dyn std::error::Error>> {
        check_search("Münch", "München Hbf").await
    }

    #[tokio::test]
    async fn search_vienna() -> Result<(), Box<dyn std::error::Error>> {
        check_search("Wien", "Wien Hbf").await
    }

    #[tokio::test]
    async fn journey_munich_nuremberg() -> Result<(), Box<dyn std::error::Error>> {
        check_journey("de-DELFI_de:09162:100", "de-DELFI_de:09564:510").await
    }
}
