# Railway Motis Provider

Implementation of a Motis client for Railway.

This crate is part of [railway-backend](https://gitlab.com/schmiddi-on-mobile/railway-backend).

This is mainly useful for the [Transitous](https://transitous.org/) instance.
Documentation can be found [here](https://routing.spline.de/doc/index.html#post-/).
