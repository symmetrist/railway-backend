# Railway search.ch Provider

Implementation of the search.ch client for Railway.

This crate is part of [railway-backend](https://gitlab.com/schmiddi-on-mobile/railway-backend).

Documentation can be found [here](https://search.ch/timetable/api/help).
