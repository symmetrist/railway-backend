# Architecture

This document describes the high-level architecture of the backend of Railway.

## Levels

The overall architecture is based on three levels:

### Level 1: Core Type Definitions

The first level contains the core type definitions used throughout the entire backend and also propagating towards the frontend.
Those are placed inside `railway-core`.
This includes e.g.:

- `Provider`: This describes what functionality is expected by a provider.
- `Requester`: A simple API to request data.
- `Journey, Place, Leg, IntermediateLocation, ...`: Standardized types which will be returned by provider implementations.

### Level 2: Provider Implementations

This level implements the different providers independently.
Those are placed in separate `railway-provider-...` crates.
Those crates should use the first layer type definitions to implement `Provider`.
This level should not be used by front-ends in general, except if they want to support only one provider in particular.

### Level 3: Unification

The last level unifies all providers to a `RailwayProvider`-enum.
This is done by importing all provider implementations.
This implements `Provider` itself.
Most of the code for this is automatically generated, e.g. the `Provider`-implementation, or converting from and to strings.
This level should be used by front-ends.

## Design Decisions

### Core Type Definitions Instead of Separate Types for Each Provider

As described above, this architecture requires each provider to one global definition of types.
It was also considered for every single provider to have its own type definitions and then convert those in the third layer to general type definitions.

This consideration was discarded as it would have required equivalent amount of work in general, but moves lots of code from the second to the third layer.
This would have made automatic generation of the unification step harder.
It was therefore decided to move this complexity to the second layer by forcing a single "source-of-truth" type definitions.

### General Requester Instead of Specific Requester

The backend allows for having a general requester.
This design was ported from the original `hafas-rs` and we decided to keep it instead of forcing the use of one provider.

While forcing one provider would have made the implementation simpler in some ways, mostly removing generics and simpler error handling, this would be hard to change later.
Furthermore, some providers required custom certificates for communicating with the server.
This is hard to do ergonomically in many libraries, therefore a `RequestBuilder` was added to allow for such certificates and future requirements.
Finally, having a custom provider easily allows other use cases, e.g. logging all requests and responses from and to servers to allow for better debugging of the requests.

### One Feature per Provider Instead of No Features

The backend allows for enabling or disabling providers using features.
This design was ported from the original `hafas-rs` and we decided to keep it instead of using no features at all.

Having no features would simplify some code, e.g. multiple changes to the `Cargo.toml` of the API is required to add a provider.
Furthermore, the activation or deactivation of features can cause issues with compilation.
It was nevertheless decided to add features for every provider, which allows to easily add or remove supported providers by just changing the `all-providers` feature.
It also allows other clients to only compile a small subset of the code if they do not require all providers.
