# CI

This project has a CI set up.
The CI is documented here.

## Clippy

CI tests with `cargo clippy` that the source code is of good quality.
This CI may fail, but will produce a warning.

If this step produces a warning, investigate where it came from and try to fix it if you caused it.

## Format

CI tests that the source code is well-formatted.
This CI may fail, but will produce a warning.

If this step produces a warning, investigate where it came from and try to fix it if you caused it.

## Required Tests

The backend of Railway has a few required tests which must pass.
Those tests should not be flaky as they require no internet and also no I/O in general.
This CI must pass for any MR to be accepted.

## Provider Tests

The backend also includes tests which ensure that all providers continue to work as expected.
As providers may temporarily cause errors, for example DNS errors or internal server errors, those errors are not required to pass.

If this step produces a warning, investigate which provider caused it.
If it is a provider changed in a current merge request, investigate it closer and fix it.
If this step repeatedly fails for a provider, this provider should also be closer investigated.
Otherwise, this failure is seen as a one-time failure and ignored.
