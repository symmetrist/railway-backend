# Developer Documentation

This folder contains documentation aimed at developers working on `railway-backend`.
It includes:

- [Architecture Description (including design decisions)](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/doc/architecture.md?ref_type=heads)
- [Writing a Provider](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/doc/writing-a-provider.md?ref_type=heads)
- [Information about the CI](https://gitlab.com/schmiddi-on-mobile/railway-backend/-/tree/main/doc/ci.md?ref_type=heads)
