# Railway Derive API

Derive macro for [railway-api](https://crates.io/crates/railway-api).

This crate is part of [railway-backend](https://gitlab.com/schmiddi-on-mobile/railway-backend).
